﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameController : MonoBehaviour
{
    public Transform DropZone;
    public GameObject InfoPanel;
    public GameObject InfoButton;
    public GameObject ScoreText;
    public GameObject HighScoreText;
    public GameObject BubblePrefab;
    public GameObject BlockPrefab;
    public GameObject Key;
    public GameObject StartButton;
    public static bool isGameStarted;
    public GameOverAnimation gameOverAnim;
    public GameObject FailText;
    public Camera myCamera;
    public GameObject[] bubbles;
    public GameObject[] blocks;

    public AudioClip AlienSound;

    public bool timeCounter;
    public bool blockCounter;

    // Use this for initialization
    void Start()
    {
        HighScoreText.GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetInt("highScoreKey").ToString();

        AudioSource.PlayClipAtPoint(AlienSound, transform.position);
        ShowGameOverAnim(gameOverAnim);

    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("isGameStarted : " + isGameStarted);
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.collider != null)
        {
            if (Input.GetMouseButtonDown(0))
            {

                if (hit.collider.name.Equals("StartButton"))
                {
                    if (isGameStarted == false)
                    {
                        Key.transform.SetParent(this.transform);
                        timeCounter = true;
                        blockCounter = true;
                        StartButton.SetActive(false);
                        InfoButton.SetActive(false);
                        isGameStarted = true;
                        Key.GetComponent<Rigidbody2D>().gravityScale = 0.3f;
                        KeyController.isFallDown = false;  
                        gameOverAnim.IsGameOver = false;
                        FailText.SetActive(false);
                        StartCoroutine(GenerateBubble());
                        GameObject clone = Instantiate(BubblePrefab) as GameObject;
                        clone.transform.SetParent(this.transform);
                        clone.transform.localScale = new Vector3(7f, 7f, 7f);
                        clone.transform.position = new Vector3(0, -3f, 90f);
                    }

                }
                else if (hit.collider.name.Equals("InfoPanel"))
                {
                    InfoPanel.SetActive(false);
                }
                else if (hit.collider.name.Equals("InfoButton"))
                {
                    InfoPanel.SetActive(true);
                }
            }
        }

        if (KeyController.isFallDown == true)
        {

            timeCounter = false;
            blockCounter = false;
            StopAllCoroutines();

            StartCoroutine(ShowGameOverAnimation());
            


        }

        if (isGameStarted == true)
        {
            StartCoroutine(StartCounter());
            StartCoroutine(GenerateBlocks());

        }
        else
        {
            timeCounter = false;
            blockCounter = false;

        }
    }

    IEnumerator GenerateBubble()
    {
        while (isGameStarted == true)
        {
            Debug.Log("Game is started and Generating bubble");
            yield return new WaitForSeconds(2f);

            GameObject clone = Instantiate(BubblePrefab) as GameObject;
            float suitableX = UnityEngine.Random.Range(myCamera.transform.position.x - 2.7f, myCamera.transform.position.x + 2.7f);
            float suitableY = UnityEngine.Random.Range(myCamera.transform.position.y - 6f, myCamera.transform.position.y + -7.5f);
            clone.GetComponent<SpriteRenderer>().color = new Color32(0, 255, 0, 255);
            clone.transform.SetParent(this.transform);
            clone.transform.localScale = new Vector3(7f, 7f, 7f);
            clone.transform.position = new Vector3(suitableX, suitableY, 90f);
            //   platformCount++;

        }
    }
    public void ShowGameOverAnim(GameOverAnimation gameover)
    {

        if (gameOverAnim != null)
            gameOverAnim.IsGameOver = true;

        gameOverAnim = gameover;
        gameOverAnim.IsGameOver = false;
    }

    IEnumerator ShowGameOverAnimation()
    {
        if (int.Parse(HighScoreText.GetComponent<TextMeshProUGUI>().text) <= int.Parse(ScoreText.GetComponent<TextMeshProUGUI>().text))
        {
            HighScoreText.GetComponent<TextMeshProUGUI>().text = ScoreText.GetComponent<TextMeshProUGUI>().text;
            PlayerPrefs.SetInt("highScoreKey", int.Parse(HighScoreText.GetComponent<TextMeshProUGUI>().text));

        }
        ScoreText.GetComponent<TextMeshProUGUI>().text = "0";
        FailText.SetActive(true);
        gameOverAnim.IsGameOver = true;
        Debug.Log(gameOverAnim.IsGameOver);
        
        
        KeyController.isFallDown = false;
        bubbles = GameObject.FindGameObjectsWithTag("Bubble");
        foreach (GameObject b in bubbles)
        {
            Destroy(b);
        }
        blocks = GameObject.FindGameObjectsWithTag("Block");
        foreach (GameObject b in blocks)
        {
            Destroy(b);
        }
        if (isGameStarted == true)
        {
            Debug.Log("sıfırlıyor");
            Key.transform.SetParent(DropZone);
        
            Key.GetComponent<Rigidbody2D>().gravityScale = 0.0f;
            Key.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
        }
        isGameStarted = false;
        StartButton.SetActive(true);
        InfoButton.SetActive(true);
        yield return new WaitForSeconds(1f);

        Key.GetComponent<Rigidbody2D>().gravityScale = 0.3f;

        yield return new WaitForSeconds(1f);
      

    }

    IEnumerator StartCounter()
    {
        while (timeCounter == true && isGameStarted == true)
        {
            timeCounter = false;
            ScoreText.GetComponent<TextMeshProUGUI>().text =
                (int.Parse(ScoreText.GetComponent<TextMeshProUGUI>().text) + 1).ToString();           
            yield return new WaitForSeconds(1f);
            timeCounter = true;
        }

    }
    IEnumerator GenerateBlocks()
    {
        while (blockCounter == true  && isGameStarted == true)
        {
            blockCounter = false;
            GameObject clone = Instantiate(BlockPrefab) as GameObject;
            float suitableX = UnityEngine.Random.Range(myCamera.transform.position.x -2.7f, myCamera.transform.position.x +2.7f);
            float suitableY = myCamera.transform.position.y * 1.7f + 7.5f;
            Debug.Log(myCamera.transform.position.y);
            clone.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            clone.transform.position = new Vector3(suitableX, suitableY, 89f);
            yield return new WaitForSeconds(3.5f);
            blockCounter = true;

        }
    }
}
