﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour
{
    public GameObject InfoPanel;
    public GameObject Key;
    public GameObject EdgeTop;
    public GameObject[] bubbles;
    private Vector2 leftWindForce;
    private Vector2 rightWindForce;


    public AudioClip BubblePopSound;
    public AudioClip AlienSound;

    public Vector2 rightReferenceVector;
    public Vector2 leftReferenceVector;
    public float relativeDistance;
    // Use this for initialization
    void Start()
    {
        leftWindForce = new Vector2(10f, 0f);
        rightWindForce = new Vector2(-10f, 0f);

        rightReferenceVector = new Vector2(170f, 0f);
        leftReferenceVector = new Vector2(-170f, 0f);
    }
    // Update is called once per frame
    void Update()
    {
        bubbles = GameObject.FindGameObjectsWithTag("Bubble");

        foreach (GameObject b in bubbles)
        {
            float verticalDifference = EdgeTop.transform.position.y - b.transform.position.y;
            if (verticalDifference > 10)
            {
                b.GetComponent<SpriteRenderer>().color = new Color32(0, 255, 0, 255);
            }
            else
            {
                b.GetComponent<SpriteRenderer>().color = new Color32(((byte)(255 - verticalDifference * 25)), (byte)(verticalDifference * 25), 0, 255);

            }


        }
    }

    public void RightButtonDown()
    {

        bubbles = GameObject.FindGameObjectsWithTag("Bubble");

        foreach (GameObject b in bubbles)
        {
            relativeDistance = rightReferenceVector.x / 85f - b.transform.position.x;
            //     Debug.Log("relativedistance" +relativeDistance);
            float maximumSpeed = 5f;
            float correctSpeed = (maximumSpeed - relativeDistance);
            b.GetComponent<BubbleController>().MoveSpeedX = -correctSpeed / 3f;
            //  Debug.Log("correctspeed"+ -correctSpeed / 2f);
        }

    }
    public void LeftButtonDown()
    {
        bubbles = GameObject.FindGameObjectsWithTag("Bubble");
        foreach (GameObject b in bubbles)
        {
            relativeDistance = b.transform.position.x - leftReferenceVector.x / 85f;
            //     Debug.Log("relativedistance" +relativeDistance);
            float maximumSpeed = 5f;
            float correctSpeed = (maximumSpeed - relativeDistance);
            b.GetComponent<BubbleController>().MoveSpeedX = correctSpeed / 3f;
            //  Debug.Log("correctspeed"+ -correctSpeed / 2f);
        }
    }
    public void PopButtonDown()
    {
        bubbles = GameObject.FindGameObjectsWithTag("Bubble");
        foreach (GameObject b in bubbles)
        {
            if (b.GetComponent<BubbleController>().isBubbleContainsKey == true)
            {
                AudioSource.PlayClipAtPoint(AlienSound, transform.position);
                Key.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0.5f;
                Key.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                AudioSource.PlayClipAtPoint(BubblePopSound, transform.position);
                b.GetComponent<BubbleController>().isBubbleContainsKey = false;
                Destroy(b);
            }

        }
    }
    public void RightButtonUp()
    {
        // StartCoroutine(RightButtonRelease());
        bubbles = GameObject.FindGameObjectsWithTag("Bubble");
        foreach (GameObject b in bubbles)
        {
            b.GetComponent<BubbleController>().MoveSpeedX = 0f;
        }
    }
    public void LeftButtonUp()
    {
        //   StartCoroutine(LeftButtonRelease());
        bubbles = GameObject.FindGameObjectsWithTag("Bubble");
        foreach (GameObject b in bubbles)
        {
            b.GetComponent<BubbleController>().MoveSpeedX = 0f;
        }
    }
   
    /*
        IEnumerator RightButtonRelease()
        {
            bubbles = GameObject.FindGameObjectsWithTag("Bubble");
            /*       foreach (GameObject b in bubbles)
                   {
                       b.GetComponent<BubbleController>().MoveSpeedX = -0.75f;
                   }
                   yield return new WaitForSeconds(0.1f);
                   foreach (GameObject b in bubbles)
                   {
                       b.GetComponent<BubbleController>().MoveSpeedX = -0.25f;
                   }

            yield return new WaitForSeconds(0f);
            foreach (GameObject b in bubbles)
            {
                b.GetComponent<BubbleController>().MoveSpeedX = 0f;
            }
        }
    */
    /*
        IEnumerator LeftButtonRelease()
        {
            bubbles = GameObject.FindGameObjectsWithTag("Bubble");
            /*  foreach (GameObject b in bubbles)
              {
                  b.GetComponent<BubbleController>().MoveSpeedX = 0.75f;
              }
              yield return new WaitForSeconds(0.1f);      
              yield return new WaitForSeconds(0.1f);
              foreach (GameObject b in bubbles)
              {
                  b.GetComponent<BubbleController>().MoveSpeedX = 0.25f;
              }

            yield return new WaitForSeconds(0f);
            foreach (GameObject b in bubbles)
            {
                b.GetComponent<BubbleController>().MoveSpeedX = 0f;
            }
        }
      */
}
