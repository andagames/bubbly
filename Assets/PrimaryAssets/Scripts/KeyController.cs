﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyController : MonoBehaviour
{
    public GameObject StartButton;
    public GameObject[] bubbles;
    private Rigidbody2D myRigidBody;
    public static bool isFallDown;
    public AudioClip BubblePopSound;
    public AudioClip AlienSound;
    // Use this for initialization
    void Start()
    {
        isFallDown = false;
        myRigidBody = GetComponent<Rigidbody2D>();       
        myRigidBody.gravityScale = 0.5f;       

    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.isGameStarted == true)
        {
            bubbles = GameObject.FindGameObjectsWithTag("Bubble");
            foreach (GameObject b in bubbles)
            {
                if (b.GetComponent<BubbleController>().isBubbleContainsKey == true)
                {

                    this.transform.position = b.transform.position;
                    //this.gameObject.GetComponent<Rigidbody2D>().velocity = b.gameObject.GetComponent<Rigidbody2D>().velocity;
                }
            }
        }

    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log(collider.gameObject);
        if (collider.gameObject.tag == "Bubble")
        {
            collider.GetComponent<BubbleController>().isBubbleContainsKey = true;
            AudioSource.PlayClipAtPoint(BubblePopSound, transform.position);
            myRigidBody.gravityScale = 0f;

            this.gameObject.transform.position = collider.transform.position;
            myRigidBody.velocity = new Vector2(0, 0);
            myRigidBody.velocity = collider.gameObject.GetComponent<Rigidbody2D>().velocity;

        }
        else if (collider.gameObject.tag == "EdgeBottom")
        {
            isFallDown = true;
            myRigidBody.velocity = new Vector2(0, 0);
        }
        else if(collider.gameObject.tag == "Block")
        {
            foreach (GameObject b in bubbles)
            {
                if (b.GetComponent<BubbleController>().isBubbleContainsKey == true)
                {
                    b.GetComponent<BubbleController>().isBubbleContainsKey = false;
                    myRigidBody.gravityScale = 0.5f;
                    AudioSource.PlayClipAtPoint(AlienSound, transform.position);

                }
            }
        }
    }
   
}
