﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleController : MonoBehaviour
{

    private GameObject Key;
    private Rigidbody2D myRigidBody;
    public float MoveSpeedX;
    public float MoveSpeedY;
    public bool isBubbleContainsKey;
    public AudioClip BubblePopSound;
    public AudioClip AlienSound;
    //public GameObject Key;
    // Use this for initialization
    void Start()
    {
        Key = GameObject.FindGameObjectWithTag("Key");
        myRigidBody = GetComponent<Rigidbody2D>();
       // MoveSpeedX = 0;
        MoveSpeedY = 1.3f;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.isGameStarted == true)
        {
            myRigidBody.velocity = new Vector2(MoveSpeedX, MoveSpeedY);
        }

    }

    void OnMouseDown()
    {

        if (GameController.isGameStarted == true)
        {
            Destroy(this.gameObject);
            if (isBubbleContainsKey == true)
            {
                AudioSource.PlayClipAtPoint(AlienSound, transform.position);
                isBubbleContainsKey = false;
                Key.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0.5f;
                Key.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                AudioSource.PlayClipAtPoint(BubblePopSound, transform.position);

            }
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {

    
        if (collider.gameObject.tag == "Edge")
        {
            Destroy(this.gameObject);
            AudioSource.PlayClipAtPoint(BubblePopSound, transform.position);
            if (isBubbleContainsKey == true)
            {
                isBubbleContainsKey = false;
                Key.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0.5f;
                AudioSource.PlayClipAtPoint(AlienSound, transform.position);

            }

        }
    }
}
