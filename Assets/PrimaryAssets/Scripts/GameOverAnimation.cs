﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverAnimation : MonoBehaviour {
    private Animator anim;

    public bool IsGameOver
    {
        get { return anim.GetBool("IsGameOver"); }
        set { anim.SetBool("IsGameOver", value); }
    }
    public void Awake()
    {
        anim = GetComponent<Animator>();
    }

}


