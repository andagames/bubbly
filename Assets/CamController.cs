﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamController : MonoBehaviour {

    private Rigidbody2D myRigidBody;
    public float MoveSpeedX;
    public float MoveSpeedY;
    // Use this for initialization
    void Start () {
        myRigidBody = GetComponent<Rigidbody2D>();
        MoveSpeedX = 0;
        MoveSpeedY = 0.6f;
    }
	
	// Update is called once per frame
	void Update () {
        if (GameController.isGameStarted == true)
        {
            myRigidBody.velocity = new Vector2(MoveSpeedX, MoveSpeedY);
        }
        else
        {
            myRigidBody.velocity = new Vector2(0, 0);
            myRigidBody.transform.position = new Vector3(0, 0,-10f);
        }

    }
}
